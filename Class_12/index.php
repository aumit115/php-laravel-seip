<?php

echo "Hello Bangladesh";
//variable
$number = 10;
$number = 20;
 echo $number;
echo "<br>";
 //constant
 define('APP_NAME', 'ERP');
 echo APP_NAME;
 echo "<br>";

 //Data type
 // 10 types of data; can be divide into 3 types-> scalar(int,float,boolean,
 //string), compound(Array,Object,Callable,Iterable), special(Null, Resource)
 //Now let's see scalar data type & print its data type
 $var = 10;
 var_dump($var);
 $var1 = 10.7;
 var_dump($var);
 $var2 = 'Aumit';
 var_dump($var2);


 // Compound data type
 //Array
 $arr = array(10, 20, 30, 40, 50, 60);
 $arr1 = [1,2,3,4,5,6,7,8,9];

 //Object type
 class Person
 {
     public $fn = 'Aumit';public $ln = 'Hasan';
 }

 $personObject = new Person();
 var_dump($personObject);


 //Special data type
 //Null
 $vip = null;
 var_dump($vip);


 //pi
 define('PI', 3.1416);//USER DEFINE CONSTANT

 echo __LINE__;//magic constant
 echo __FILE__;//magic constant
 echo __DIR__;//magic constant

 //cONVENTION -> Global standard 
 $userName = 'Meaningfull/Conventional variable name eta';

 $var79 = true;
 $var46 = 899;
 echo $var79;//print 1 but for false it will print nothing
//Variable Handling function
 echo empty($var79);//print 1; it is called variable handling function
 echo isset($var79);
 echo gettype($var79);
 unset($var46);
// Error handling function
// empty, is_array, is_null, isset, print_r, serialize, var_dump,
//array_key_exists, unset, gettype, is_int, is_float

 //if-else

 if(true){
     echo "hi";
 }

 if(isset($var79)){
     echo "hello";
 }


echo '<pre>';
print_r($arr);
//echo & print is not for compound variable
//php is interpreted language that's why it's called scripting language
// interpret kore zend engine
echo array_key_exists(1, $var79);//index/key check kore
$serializeArray = serialize($array);//ekta array k string a transform kore
print_r(unserialize($serializeArray));


?>
